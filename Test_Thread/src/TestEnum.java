import jdk.nashorn.internal.objects.annotations.Getter;


public enum TestEnum {

    CON_TE(1,"troi dep"),
    CON_TTT(2,"xau ngaoc"),
    CON_ESE(3,"xn xawn"),
    CON_TEDFS(4,"long lang");
    private String tr;
    private Integer a;

    TestEnum(Integer a,String tr) {
        this.tr = tr;
        this.a = a;
    }

    public String getTr() {
        return tr;
    }

    public Integer getA() {
        return a;
    }
}
